import Head from 'next/head'
import Image from 'next/image'
import LoginComponent from '../component/auth/login/login-component'
import AuthLayoutComponent from '../component/layouts/auth-layout/auth-layout.component'
import styles from '../styles/Home.module.css'
import LoginFirstPage from '../component/auth/login-first-img/login-first-page'

export async function getServerSideProps(context) {
  return {
      props: {
          query: context.query
      }
  }
}

export default function Home(query) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Leasing Monk - Work Near Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <AuthLayoutComponent home>
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-7">
                  <LoginFirstPage query={query}/>
                </div>
                <div className="col-lg-8 col-md-8 col-sm-8">
                  <LoginComponent query={query}/>
                </div>
              </div>
        </AuthLayoutComponent>
      </main>

    </div>
  )
}
