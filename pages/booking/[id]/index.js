import Head from 'next/head'
import React from 'react'
import BookingDetail from '../../../component/booking/booking-detail'
import NewBooking from '../../../component/booking/new-booking'
import SidebarDashboardLayout from '../../../component/layouts/sidebar-layout/sidebar-dashboard-layout'

export async function getServerSideProps(context) {
    return {
        props: {
            query: context.query.id
        }
    }
}

export default function Booking({query}) {
  return (
        <section>
            <React.Fragment>
                <Head>
                    <title>Leasing Monk - Booking</title>
                </Head>
            </React.Fragment>
            <SidebarDashboardLayout>
               {query ==="new-booking" && <NewBooking/>}
               {query ==="booking-detail" && <BookingDetail/>}
            </SidebarDashboardLayout>
        </section>
  )
}
