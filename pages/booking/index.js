import Head from 'next/head'
import React, { Component } from 'react'
import BookingComponent from '../../component/booking/booking'
import SidebarDashboardLayout from '../../component/layouts/sidebar-layout/sidebar-dashboard-layout'


export class Booking extends Component {
  render() {
    return (
        <section>
            <React.Fragment>
                <Head>
                    <title>Leasing Monk - Booking</title>
                </Head>
            </React.Fragment>
            <SidebarDashboardLayout>
                <BookingComponent/>
            </SidebarDashboardLayout>
        </section>
    )
  }
}

export default Booking