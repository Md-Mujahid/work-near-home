// import '../styles/globals.css'
import '../styles/component-styles.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
// import '../styles/sidebar-layout-styles.scss'
// import '../styles/auth-styles.scss'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
