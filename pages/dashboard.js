import Head from 'next/head'
import React from 'react'
import Dashboard from '../component/dashboard/dashboard/dashboard'
import NewUserDashboard from '../component/dashboard/new-user-dashboard/new-user-dashboard'
import SidebarDashboardLayout from '../component/layouts/sidebar-layout/sidebar-dashboard-layout'

export async function getServerSideProps(context) {
    return {
        props: {
            query: context.query
        }
    }
}

export default function DashboardPage({query}) {
  return (
    <section>
        <React.Fragment>
            <Head>
                <title>Leasing Monk - Work Near Home</title>
            </Head>
        </React.Fragment>
        <SidebarDashboardLayout>
            {/* <NewUserDashboard/> */}

            <Dashboard/>
        </SidebarDashboardLayout>
        
    </section>
  )
}
