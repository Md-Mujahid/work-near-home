import React from 'react'
import SidebarDashboardLayout from '../component/layouts/sidebar-layout/sidebar-dashboard-layout'
import ConfirmBooking from '../component/booking/confirm-booking/confirm-booking'
import WorkingDays from '../component/booking/confirm-booking/working-days'
import CompanyProfile from '../component/company/company-profile/company-profile'
import FinalBooking from '../component/booking/confirm-booking/final-booking'
import MobileViewSidebar from '../component/auth/sidebar/mobile-view-sidebar'

export default function demo() {
  return (
    <section>
      {/* <SidebarDashboardLayout> */}
        {/* <CompanyProfile/> */}
        {/* <ConfirmBooking/> */}

        {/* <WorkingDays/>  */}

        {/* <FinalBooking/> */}

      {/* </SidebarDashboardLayout> */}

        <MobileViewSidebar/>
    </section>
  )
}
