import React, { Component } from 'react'
import Head from 'next/head';
import styles from '../../styles/Home.module.css'
import LoginFirstPage from '../../component/auth/login-first-img/login-first-page';
import SignUp from '../../component/auth/signup/sign-up';
import AuthLayoutComponent from '../../component/layouts/auth-layout/auth-layout.component';

export async function getServerSideProps(context) {
    return {
        props: {
            query: context.query
        }
    }
}


export class SignUpPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
           query:props.query
        };
    }

  render() {
    return <section >
        <React.Fragment>
            <Head>
                <title>Leasing Monk - Work Near Home</title>
            </Head>
        </React.Fragment>
        <main className={styles.main}>
            <AuthLayoutComponent home query={this.state.query}>
                <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-3">
                        <LoginFirstPage query={this.state.query}/>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-9">
                        <SignUp query={this.state.query}/>
                    </div>
                </div>
            </AuthLayoutComponent>
        </main>

    </section>
  }
}

export default SignUpPage