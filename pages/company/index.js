import Head from 'next/head'
import React, { Component } from 'react'
import EditComponyProfile from '../../component/company/edit-company-profile/edit-company-profile'
import SidebarDashboardLayout from '../../component/layouts/sidebar-layout/sidebar-dashboard-layout'

export class Company extends Component {
  render() {
    return (
        <section>
            <React.Fragment>
                <Head>
                    <title>Leasing Monk - Edit Company Profile</title>
                </Head>
            </React.Fragment>
            <SidebarDashboardLayout>
                <EditComponyProfile/>
            </SidebarDashboardLayout>
        </section>
    )
  }
}

export default Company