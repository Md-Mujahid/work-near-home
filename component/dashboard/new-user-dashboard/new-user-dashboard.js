import Link from 'next/link'
import React, { Component } from 'react'

export class NewUserDashboard extends Component {
  render() {
    return (
        <div data-Component="new-user-dashboard">
            <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12 text-center dasboard-margin'>
                    <h1 className='dashboard-heading'>Welcome to your dashboard, Mark</h1>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12 mt-2 text-center'>
                    <p className='sub-heading'>Get Started by Setting up your company profile.Click on the button below...</p>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12 mt-5 text-center'>
                    <Link href="/company">
                        <button class="btn btn-primary started-btn"  type="button">Get Started</button>
                    </Link>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12 text-center'>
                    <img src='/images/dashboard-img.PNG' alt='Leasing Monk' className='img-fluid dashboard-page-img'/>
                </div>
            </div>
        </div>
    )
  }
}
export default NewUserDashboard
