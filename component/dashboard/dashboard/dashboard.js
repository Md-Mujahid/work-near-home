import React, { Component } from 'react'
import EmployeeAnalytics from '../dashboard-item/employee-analytics'
import MakeNewBookingDashboard from '../dashboard-item/make-new-booking-dashboard'
import RecentActivities from '../dashboard-item/recent-activities'
import ScheduleBookingDashboard from '../dashboard-item/schedule-booking-dashboard'
import dashboard from '../../../src/dashboard.json'


export class Dashboard extends Component {
  render() {
    console.log(dashboard,'123')
    return ( 
        <div data-Component="dashboard">
            <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12 p-4'>
                    <h1 className='dashboard-heading'>Welcome to your dashboard, Mark</h1>
                </div>
            </div>

            <div className='row'>

                <div className='col-lg-6 col-md-6 col-sm-6  mt-3'>
                    <div className='shadow dashboard-component-background'>
                        <MakeNewBookingDashboard/>
                    </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-6 mt-3'>
                    <div className='shadow dashboard-component-background'>
                        <ScheduleBookingDashboard/>
                    </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-6 mt-3'>
                    <div className='shadow dashboard-component-background'>
                        <RecentActivities/>
                    </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-6 mt-3'>
                    <div className='shadow dashboard-component-background'>
                        <EmployeeAnalytics/>
                    </div>
                </div>

            </div>

        </div>
    )
  }
}

export default Dashboard