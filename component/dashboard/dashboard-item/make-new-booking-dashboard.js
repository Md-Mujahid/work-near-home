import Link from 'next/link'
import React, { Component } from 'react'
import dashboard from '../../../src/dashboard.json'

export class MakeNewBookingDashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: dashboard,
            selectedOption: 'None',
        }
    }


    dropDowns() {
       for(var i=0; i < this.state.data.length; i++) {
            var state = this.state.data[i].state;
        }return state;
    }

    handleChange = ({ target }) => {
        this.setState({
            selectedOption: target.value,
        });
    }

  render() {
    console.log(this.state.data, 456)
    return (
        <div data-Component="dashboard">
            <div className='dashboard-new-booking-page'>
            {this.state.data.map(value=>{
                return <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <div className='border-3 p-3 border-bottom'>
                            <span className='heading'>Make a new booking</span>
                        </div>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 p-3'>
                        <p className='sub-heading'>Select booking type </p>
                    </div>

                     <div className='col-lg-12 col-md-12 col-sm-12'>
                        <div className='row'>
                            <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <Link href="#">
                                    <button className='btn btn-primary pass-btn hourly-pass-btn' type='button'>{value.new_booking.select_booking_type.hourly}</button>
                                </Link>
                            </div>

                            <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <button className='btn btn-primary pass-btn daily-pass-btn' type='button'>{value.new_booking.select_booking_type.daily}</button>
                            </div>

                            <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <button className='btn btn-primary pass-btn monthly-pass-btn' type='button'>{value.new_booking.select_booking_type.monyhly}</button>
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 mt-4'>
                        <p className='sub-heading'>Select space location & types</p>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 mb-2'>
                        <div className='row'>
                        
                        <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <select class="form-select dropdown-width" aria-label="Default select example" onChange={this.handleChange}>
                                    <option selected>State</option>
                                    <option value={value.new_booking.space_location?.states} >{value.new_booking.space_location?.states}</option>
                                </select> 
                            </div>

                            <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <select class="form-select dropdown-width" aria-label="Default select example" onChange={this.handleChange}>
                                    <option selected>City</option>
                                    <option value={value.new_booking.space_location?.cities.city}>{value.new_booking.space_location?.cities.city}</option>
                                    
                                </select>
                            </div>

                            <div className='col-lg-4 col-md-4 col-sm-4 col'>
                                <select class="form-select dropdown-width" aria-label="Default select example">
                                    <option selected>Space type</option>
                                    <option value="1">{value.new_booking.space_type}</option>
                                </select>
                            </div>

                        </div>
                    </div>


                    <div className='col-lg-12 col-md-12 col-sm-12 mt-4 mb-5 text-end'>
                        <button className='btn btn-primary find-me-space-btn' type='button'>Find me space</button>
                    </div>

                </div>   
            })}   
            </div>
        </div>
    )
  }
}

export default MakeNewBookingDashboard