import Link from 'next/link'
import React, { Component } from 'react'

export class ScheduleBookingDashboard extends Component {
  render() {
    return (
        <div data-Component="dashboard">
            <div className='dashboard-new-booking-page'>
                <div className='row'>

                    <div className='col-lg-12 col-md-12 col-am-12'>
                        <div className='border-3 p-3 border-bottom'>
                            <div className='row'>
                                <div className='col-lg-10 col-md-10 col-sm-10 col'>
                                    <span className='heading'>Scheduled booking</span>
                                </div>
                                <div className='col-lg-2 col-md-2 col-sm-2 col text-end'>
                                    <Link href={`/booking/${`booking-detail`}`}>
                                        <a className='view-all'>View all</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 p-3'>
                        <div className='row'>
                            <div className='col-2'>
                                <p className='dashboard-text pad'>All</p>
                            </div>
                            <div className='col-2'>
                                <p className='dashboard-text'>This week</p>
                            </div>
                            <div className='col-2'>
                                <p className='dashboard-text'>This month</p>
                            </div>
                            <div className='col-2'>
                                <p className='dashboard-text'>Last month</p>
                            </div>
                            <div className='col-2'>
                                <p className='dashboard-text'>Last quarter</p>
                            </div>
                            <div className='col-2'>
                                <p className='dashboard-text'>Last year</p>
                            </div>
                        </div>
                    </div>


                    <div className='col-lg-12 col-md-12 col-sm-12'>
                    </div>

                </div>
            </div>
        </div>
    )
  }
}

export default ScheduleBookingDashboard