import React, { Component } from 'react'
import { ProgressBar } from 'react-bootstrap'

export class EmployeeAnalytics extends Component {
    constructor(props){
        super(props);
        this.state ={
            
        }
    }

  render() {
    return (
        <div data-Component="employee-analytics">
            <div className='dashboard-new-booking-page mb-2'>
                <div className='row'>

                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <div className='border-3 p-3 border-bottom'>
                            <div className='row'>
                                <div className='col-7'>
                                    <span className='heading'>Employee Analytics</span>
                                </div>
                                <div className='col-5 text-end'>
                                    <a className='view-all'>View all employees</a>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div className='col-lg-12 col-md-12 col-sm-12 p-3'>
                        <p className='sub-heading-I'>Total Active employees - 334</p>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 mb-2'>
                        <div className='progress-bar'>
                            <ProgressBar>
                                <ProgressBar variant="primary" now={40}/>
                                <ProgressBar variant="success" now={35} key={1} />
                                <ProgressBar variant="warning" now={20} key={2} />
                                <ProgressBar variant="danger" now={10} key={3} />
                            </ProgressBar>
                        </div>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 mt-2'>
                        <div className='row'>
                            <div className='col-4'>
                                <div className='circle'> </div>
                                <span className='progress-bar-text text-muted'>Tecnology</span>
                            </div>

                            <div className='col-4'>
                                <span className='progress-nmbr text-muted'>90</span>
                            </div>
                            <div className='col-4'>
                                <div className='number-circle'>
                                    <span className='percent-number'>27%</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='col-md-12 mt-2'>
                        <div className='row'>
                            <div className='col-md-4'>
                                <div className='circle'> </div>
                                <span className='progress-bar-text text-muted'>Tecnology</span>
                            </div>

                            <div className='col-md-4'>
                                <span className='progress-nmbr text-muted'>90</span>
                            </div>
                            <div className='col-md-4'>
                                <div className='number-circle'>
                                    <span className='percent-number'>27%</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className='col-md-12 mt-2'>
                        <div className='row'>
                            <div className='col-md-4'>
                                <div className='circle'> </div>
                                <span className='progress-bar-text text-muted'>Tecnology</span>
                            </div>

                            <div className='col-md-4'>
                                <span className='progress-nmbr text-muted'>90</span>
                            </div>
                            <div className='col-md-4'>
                                <div className='number-circle'>
                                    <span className='percent-number'>27%</span>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div className='col-md-12 mt-2 mb-3'>
                        <div className='row'>
                            <div className='col-md-4'>
                                <div className='circle'> </div>
                                <span className='progress-bar-text text-muted'>Tecnology</span>
                            </div>

                            <div className='col-md-4'>
                                <span className='progress-nmbr text-muted'>90</span>
                            </div>
                            <div className='col-md-4'>
                                <div className='number-circle'>
                                    <span className='percent-number'>27%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
  }
}

export default EmployeeAnalytics