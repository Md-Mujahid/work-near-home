import React, { Component } from 'react'

export class RecentActivities extends Component {
  render() {
    return (
        <div data-Component="dashboard">
            <div className='dashboard-new-booking-page mb-2'>
                <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <div className='border-3 p-3 border-bottom'>
                            <span className='heading'>Recent activities</span>
                        </div>
                    </div>

                    <div className='col-lg-12 col-md-12 col-sm-12 scroll'>
                       
                        <div class="card card-padding mt-5">
                            <div class="card-body">
                                <div className='row'>
                                    <div className='col-6'>
                                        <h5 class="card-title">Rohit Sharma</h5>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-textI text-end text-muted">Check-in:</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-muted">FBC 1193</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-end text-muted">Today, 9.57 AM</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-muted">Bengaluru Creative Design Team</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-end text-muted">Indicube Coworking space</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-muted">Senior creative designer</p>
                                    </div>

                                    <div className='col-6'>
                                        <p class="card-text text-end text-muted">Sarjapur Road, Bengaluru</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card card-padding mt-4">
                            <div class="card-body">
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <h5 class="card-title">Rohit Sharma</h5>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-textI text-end text-muted">Check-in:</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">FBC 1193</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Today, 9.57 AM</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">Bengaluru Creative Design Team</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Indicube Coworking space</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">Senior creative designer</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Sarjapur Road, Bengaluru</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card card-padding mt-4 mb-4">
                            <div class="card-body">
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <h5 class="card-title">Rohit Sharma</h5>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-textI text-end text-muted">Check-in:</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">FBC 1193</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Today, 9.57 AM</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">Bengaluru Creative Design Team</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Indicube Coworking space</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-muted">Senior creative designer</p>
                                    </div>

                                    <div className='col-md-6'>
                                        <p class="card-text text-end text-muted">Sarjapur Road, Bengaluru</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
  }
}

export default RecentActivities