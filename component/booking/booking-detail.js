import Link from 'next/link'
import React, { Component } from 'react'

export class BookingDetail extends Component {
  render() {
    return (
        <div data-Component="booking-detail">
            <div className='booking-detail-page mb-3'>
                <div className='row'>

                    <div className='col-md-12 pt-3'>
                        <div className='row'>
                            <div className='col-md-6'>
                                <p className='booking-heading'>Booking</p>
                            </div>
                            <div className='col-md-4'>
                                <div class="input-group mb-3">
                                    <span class="input-group-text search-icon" id="basic-addon1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-search search-icon" viewBox="0 0 16 16">
                                            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                        </svg>
                                    </span>
                                    <input type="text" class="form-control" placeholder="Search for your employee here..." aria-label="Username" aria-describedby="basic-addon1" />
                                </div>
                            </div>
                            <div className='col-md-2 text-end'>
                                <Link href={`/booking/${`new-booking`}`}>
                                    <button className='btn btn-primary filter-btn'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-lg icon-plus" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                    </svg>
                                    New Booking
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className='sub-booking-detail-page'>
                <div className='row shadow'>

                    <div className='col-md-12'>
                        <div className='border-3 p-2 border-bottom'>
                            <div className='row'>
                                <div className='col-md-7'>
                                    <span className='heading'>Total of 148 booking found.....</span>
                                </div>
                                <div className='col-md-1'>
                                    <button className='btn btn-primary filter-booking-btn'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-funnel-fill funnel" viewBox="0 0 16 16">
                                            <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2z"/>
                                        </svg>
                                        Filter
                                    </button>
                                </div>
                                <div className='col-md-2'>
                                    <div className='dropdown-box'>
                                        <select class="form-select dropdown" aria-label="Default select example">
                                            <option selected>Pass type</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div className='col-md-2'>
                                    <div className='dropdown-box box-2'>
                                        <select class="form-select dropdown drop" aria-label="Default select example">
                                            <option selected>Sort by: Newest first</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='col-md-12'>
                            <table className="table mb-0 table-lm '">
                                <tbody className='table-responsive'>
                                    <tr>
                                    <td>Request date</td>
                                    <td>Booking Id</td>
                                    <td>Booked by</td>
                                    <td>Pass type</td>
                                    <td>Booked space/s</td>
                                    <td>Location</td>
                                    <td>Created used</td>
                                    <td>status</td>
                                    <td></td>
                                    </tr>
                                </tbody>

                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>

                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>



                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>

                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>

                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>


                                <tbody>
                                    <tr>
                                        <td>27 Aug, 2022</td>
                                        <td>LM-00145272410</td>
                                        <td>You</td>
                                        <td>
                                            <div className='table-label'>
                                                Monthly Pass
                                            </div>
                                        </td>
                                        <td>
                                            ABC Workspaces 
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td>
                                            Banglore
                                                <span class="badge rounded-pill margin text-bg-secondary">
                                                    +1
                                                </span>
                                        </td>
                                        <td className='text-primary'>
                                            1,20,600 
                                        </td>
                                        <td>
                                            <div className='table-label'>
                                                Schedule
                                            </div>
                                        </td>
                                        <td>
                                            <button className='btn btn-primary view-btn'>View Details
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill arrow" viewBox="0 0 16 16">
                                                    <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>


                    </div>

                    

                </div>
            </div>

        </div>
    )
  }
}

export default BookingDetail