import React, { Component } from 'react'
import { Modal, ModalBody } from 'react-bootstrap';
import ConfirmBookingFilter from '../booking-filter/confirm-booking-filter';

export class ConfirmBooking extends Component {
    constructor(props){
        super(props);
        this.state = {
            isOpen: false,
        }
    }


    toggleModalClose = () => { 
        this.setState({
        isOpen: false
        })
      }
    
    toggleModal = () => { 
        this.setState({
        isOpen: true
        })
    }


  render() {
    return (
        <div data-Component="confirm-booking">
            <div className='confirm-booking-page mb-4'>

                <div className='row head-row'>
                    <div className='col-md-12'>
                        <div className='row'>
                            <div className='col-md-8'>
                                <p className='confirm-heading'>Company Profile</p>
                            </div>
                            <div className='col-md-4 text-end'>
                                <button className='btn btn-primary cancel-btn'>Cancel</button>
                                <button className='btn btn-primary save-btn'>Confirm booking</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='row head-row'>

                    <div className='col-md-3 col-3-padding'>
                        <div className='head-card'>
                            <span>3 Spaces selected</span>
                        </div>

                        <div className='card-width'>
                            <div class="card card-border card-selected">
                                <img src="/images/coworking-space-3.jpg" class="card-img-top img-radius" alt="Card Img"/>
                                <div class="card-body p-4">
                                    <h5 class="card-title">Abc Workspace</h5>
                                    <p class="card-text address-text">Khardi, Pune</p>
                                </div>
                                <span className='label'>Standard</span>
                            </div>
                        </div>

                        <div className='card-width mt-4'>
                            <div class="card card-border card-selected">
                                <img src="/images/coworking-space-3.jpg" class="card-img-top img-radius" alt="Card Img"/>
                                <div class="card-body p-4">
                                    <h5 class="card-title">Abc Workspace</h5>
                                    <p class="card-text address-text">Khardi, Pune</p>
                                </div>
                                <span className='label'>Standard</span>
                            </div>
                        </div>

                        <div className='card-width mt-4'>
                            <div class="card card-border card-selected">
                                <img src="/images/coworking-space-3.jpg" class="card-img-top img-radius" alt="Card Img"/>
                                <div class="card-body p-4">
                                    <h5 class="card-title">Abc Workspace</h5>
                                    <p class="card-text address-text">Khardi, Pune</p>
                                </div>
                                <span className='label'>Standard</span>
                            </div>
                        </div>

                    </div>

                    <div className='col-md-9 col-9-padding'>
                        <div className='company-page mb-5'>
                            <div className='row'>

                                <div className='col-md-3 mt-5'>
                                    <label class="form-label label-text">Booking duration (in days)</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                        <span class="input-group-text" id="basic-addon2">days</span>
                                    </div>
                                </div>

                                <div className='col-md-3 mt-5'>
                                    <label class="form-label label-text">Expected Move-in Date</label>
                                    <div class="input-group mb-3">
                                        <input type="date" class="form-control" placeholder="9 Oct, 2022" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                        
                                    </div>
                                </div>

                                <div className='col-md-3 mt-5'>
                                    <label class="form-label label-text">Lock-in Period</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="6" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                        <span class="input-group-text" id="basic-addon2">Months</span>
                                    </div>
                                </div>

                                <div className='col-md-3 mt-5'>
                                    <label class="form-label label-text">Billing Cycle</label>
                                    <div class="input-group mb-3">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Monthly</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>

                                <div className='col-md-12 mt-5'>
                                    <p class="head-text">Add employees/teams</p>
                                    <p className="head-text font mb-3">Assign employees/teams who needs to access the allocated space</p>
                                    <button className='btn btn-primary add-btn' type='button' onClick={this.toggleModal}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                        </svg>
                                    </button>
                                </div>


                                <div className='col-md-12 mt-5'>
                                    <p class="head-text">Select required products and its quantity</p>
                                    <p className="head-text font mb-3">Customize your seat package by selecting individual product quantity as per the requirement</p>
                                    <div class="input-group mb-3">
                                        <select class="form-select" aria-label="Default select example">
                                            <option disabled selected>Total seats required</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>

                                <div className='col-md-12 mt-5 mb-5'>
                                    <p class="head-text">Max user limit (per day)</p>
                                    <p className="head-text font mb-3">you'll be charged additionally per user on exceeding the daily uer limit</p>
                                    <div className='row'>
                                        <div className='col-md-4'>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                                <span class="input-group-text" id="basic-addon2">days</span>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <div className='footer'>
                <div className='col-md-12 p-4 shadow-top'>
                    <div className='row'>
                        <div className='col-md-9'>
                            <span className='footer-heading'>Click "Next" to fill booking details of the above added spaces</span>
                        </div>
                        <div className='col-md-3 text-end'>
                            <button className='btn btn-primary btn-back'>back</button>
                            <button className='btn btn-primary btn-next'>Next</button>
                        </div>
                    </div>
                </div>
            </div>

            <Modal size='xl' centered show={this.state.isOpen} onHide={this.toggleModalClose} aria-labelledby="contained-modal-title-vcenter" >
                <ModalBody>
                    <ConfirmBookingFilter no={this.toggleModalClose}/>
                </ModalBody>
            </Modal>

        </div>
    )
  }
}

export default ConfirmBooking