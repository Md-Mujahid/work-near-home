import React, { Component } from 'react'

export class WorkingDays extends Component {
  render() {
    return (
        <div data-Component="working-days">
            <div className='company-page mb-5'>
                <div className='row'>

                    <div className='col-md-12'>
                        <div className='row'>
                            <div className='col-md-10 pt-2 pb-2'>
                                <p className='text-muted mb-0 heading-text'>WeWork Coworking - Mumbai | Total 40 employees selected | 20 employees / day</p>
                            </div>
                            <div className='col-2 pt-2 pb-2'>
                                <a className='assign-working-days'>Auto assign working days</a>
                            </div>
                        </div>
                    </div>

                    <div className='col-md-12'>
                        <div className='heaging-row'>
                            <div className='row'>
                                <div className='col-md-2'>
                                    <p className='text-muted text'>User Id</p>
                                </div>
                                <div className='col-md-3'>
                                    <p className='text-muted text'>Name</p>
                                </div>
                                <div className='col-md-2'>
                                    <p className='text-muted text'>Team</p>
                                </div>
                                <div className='col-md-2'>
                                    <p className='text-muted text'>Role</p>
                                </div>
                                <div className='col-md-3 text-center'>
                                    <p className='text-muted text'>Working days</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className='col-md-12 pt-2'>  
                        <div className='row'>
                            <div className='col-md-2'>
                                <p className='text-muted text'>FBC 1003</p>
                            </div>
                            <div className='col-md-3'>
                                <p className='text-muted text'>Rahul Suprabhat</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Mumbai Teach Team</p>
                            </div>
                            <div className='col-md-2'>
                                <p className='text-muted text'>Full Stack Developer</p>
                            </div>

                            <div className='col-md-3 text-center'>
                                <div className='week-days'>
                                    <p className='week-text'>M</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>T</p>
                                </div>

                                <div className='week-days selected-days'>
                                    <p className='week-text selected-text'>W</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>T</p>
                                </div>

                                <div className='week-days'>
                                    <p className='week-text'>F</p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
  }
}

export default WorkingDays