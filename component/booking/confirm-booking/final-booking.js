import React, { Component } from 'react'

export class FinalBooking extends Component {
  render() {
    return (
        <div data-Component="final-booking">
            <div className='company-page mb-5'>
                <div className='row'>

                    <div className='col-md-12'>
                        <div className='row'>
                            <div className='col-md-10 pt-2 pb-2'>
                                <p className='text-muted mb-0 heading-text'>WeWork Coworking - Mumbai | Total 40 employees selected | 20 employees / day</p>
                            </div>
                            <div className='col-2 pt-2 pb-2'>
                                <a className='assign-working-days'>Auto assign working days</a>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>
    )
  }
}

export default FinalBooking