import React, { Component } from 'react'
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import RangeSlider from 'react-bootstrap-range-slider';
import { Slider } from '@mui/material';

export class BookingFilter extends Component {
    constructor(props){
        super(props);
            this.state = {
                value: ""
        }
    }

    handleChange = (event, newValue) => {
        this.setState({value: newValue});
    };

    clearFilter() {
        this.props.no()
    }

  render() {
    return (
        <div data-Component="booking-filter">
            <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12 p-4 shadow-sm'>

                    <div className='row mb-2'>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <a className='clear-filter' onClick={()=>{this.clearFilter()}}>Clear all Filter</a>
                        </div>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-funnel-fill" viewBox="0 0 16 16">
                                    <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2z"/>
                                </svg>
                            </span>
                            <span className='heading'>Advanced Filter for Co-working spaces</span>
                        </div>
                    </div>

                </div>

                <div className='col-lg-12 col-md-12 col-sm-12 mt-2'>
                    <p className='sub-heading'>Must have Products</p>
                </div>

                <div className='col-lg-12 col-md-12 col-sm-12'>
                    <div className='row'>
                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Director Cabin</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Bussiness Suite</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Hot Desk</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Dedicated Desk</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Meeting Room</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Conference Room</span>
                            </label>
                        </div>

                        <div className='checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Event Room</span>
                            </label>
                        </div>
                    </div>
                </div>


                <div className='col-lg-12 col-md-12 col-sm-12 mt-4'>
                    <p className='sub-heading'>Products price range </p>
                </div>

                <div className='col-lg-12 col-md-12 col-sm-12'>
                    <div className='row'>
                        <div className='range-div mt-2'>
                            <label for="exampleFormControlTextarea1" class="form-label">Business Suite</label>
                            <p className='range-text'>(INR/ seat / month)</p>
                               <div className='slider-box'>
                                    <Slider
                                        // defaultValue={[20,50]}
                                        value={this.state.newValue}
                                        onChange={this.handleChange}
                                        valueLabelDisplay="on"
                                        min={1}
                                        step={1000}
                                        max={30000}
                                        className="mb-2"
                                        sx={{
                                            color: 'primary.main',
                                            '& .MuiSlider-thumb': {
                                            borderRadius: '3px',
                                            },
                                        }}
                                    />
                               </div>
                        </div>

                        <div className='range-div mt-2'>
                            <label for="exampleFormControlTextarea1" class="form-label">Business Suite</label>
                            <p className='range-text'>(INR/ seat / month)</p>
                                <div className='slider-box'>
                                    <Slider
                                        // defaultValue={[20,50]}
                                        value={this.state.newValue}
                                        onChange={this.handleChange}
                                        valueLabelDisplay="on"
                                        min={1}
                                        step={1000}
                                        max={30000}
                                        className="mb-2"
                                        sx={{
                                            color: 'primary.main',
                                            '& .MuiSlider-thumb': {
                                            borderRadius: '3px',
                                            },
                                        }}
                                    />
                                </div>
                        </div>

                        <div className='range-div mt-2'>
                            <label for="exampleFormControlTextarea1" class="form-label">Business Suite</label>
                            <p className='range-text'>(INR/ seat / month)</p>
                                <div className='slider-box'>
                                    <Slider
                                        // defaultValue={[20,50]}
                                        value={this.state.newValue}
                                        onChange={this.handleChange}
                                        valueLabelDisplay="on"
                                        min={1}
                                        step={1000}
                                        max={30000}
                                        className="mb-2"
                                        sx={{
                                            color: 'primary.main',
                                            '& .MuiSlider-thumb': {
                                            borderRadius: '3px',
                                            },
                                        }}
                                    />
                                </div>    
                        </div>

                        <div className='range-div mt-2'>
                            <label for="exampleFormControlTextarea1" class="form-label">Business Suite</label>
                            <p className='range-text'>(INR/ seat / month)</p>
                                <div className='slider-box'>
                                    <Slider
                                        // defaultValue={[20,50]}
                                        value={this.state.newValue}
                                        onChange={this.handleChange}
                                        valueLabelDisplay="on"
                                        min={1}
                                        step={1000}
                                        max={30000}
                                        className="mb-2"
                                        sx={{
                                            color: 'primary.main',
                                            '& .MuiSlider-thumb': {
                                            borderRadius: '3px',
                                            },
                                        }}
                                    />
                                </div>    
                        </div>
                    </div>
                </div>

                <div className='col-lg-12 col-md-12 col-sm-12 mt-4'>
                    <p className='sub-heading'>Working days</p>
                </div>

                <div className='col-lg-12 col-md-12 col-sm-12'>
                    <div className='row'>
                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Monday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Tuesday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Wednesday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Thursday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Friday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Saturday</span>
                            </label>
                        </div>

                        <div className='working-checkbox-div'>
                            <label className="cursor-pointer auth-footer-link">
                                <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                <span className='checkbox-text'>Sunday</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div className='col-lg-12 col-md-12 col-sm-12 col-sm-3 mt-4'>
                    <div className='row'>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <p className='sub-heading mb-0'>Internet speed range</p>
                            <p className='range-text'>(In Mbps)</p>
                        </div>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <div className='slider-box margin'>
                                <Slider
                                    // defaultValue={[20,50]}
                                    value={this.state.newValue}
                                    onChange={this.handleChange}
                                    valueLabelDisplay="on"
                                    min={1}
                                    step={10}
                                    max={500}
                                    className="mb-2"
                                    sx={{
                                        color: 'primary.main',
                                        '& .MuiSlider-thumb': {
                                        borderRadius: '3px',
                                        },
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>


                <div className='col-lg-12 col-md-12 col-sm-12 mt-4'>
                    <div className='row'>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <p>Working Hours</p>
                        </div>
                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>From</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div className='col-lg-12 col-md-12 mt-2 col-sm-12'>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>To</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div className='col-lg-12 col-md-12 mt-4 pt-2 text-center shadow-top'>
                    <button class="btn btn-primary cancel-btn" type="button" onClick={()=>{this.props.no();}}>Cancel</button>
                    <button class="btn btn-primary save-btn" type="button">Show Result</button>
                </div>

            </div>
        </div>
    )
  }
}

export default BookingFilter