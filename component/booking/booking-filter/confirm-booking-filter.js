import React, { Component } from 'react'

export default class ConfirmBookingFilter extends Component {
  render() {
    return (
      <div data-Component="confirm-booking-filter">

        <div className='row'>

            <div className='col-md-12'>
                <div className='row'>
                    <div className='col-md-8'>
                        <p className='confirm-filter-heading'>Select employees/teams from user directory</p>
                        <p className='filter-sub-heading'>8 employees selected</p>
                    </div>
                    <div className='col-md-4 text-end'>
                        <button className='btn btn-primary go-back-btn' onClick={()=>this.props.no()}>Go back</button>
                        <button className='btn btn-primary add-btn'>Add</button>
                    </div>
                </div>
            </div>

            <div className='col-md-12'>
                <div className='row'>
                    <div className='col-md-5'>
                        <div class="input-group mb-3">
                            <span class="input-group-text search-icon" id="basic-addon1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-search search-icon" viewBox="0 0 16 16">
                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </span>
                            <input type="text" class="form-control" placeholder="Search for your employee here..." aria-label="Username" aria-describedby="basic-addon1" />
                        </div>
                    </div>

                    <div className='col-md-3'>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Select Team</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
            </div>

            <div className='col-md-12'>
                <div className='row'>
                    <div className='col-md-12 border-radius'>
                        <div className='row first-row p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>User ID</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Name</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Role</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Email</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Contact</span>
                            </div>
                            
                        </div>

                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>

                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>


                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>

                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>

                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>


                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>


                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>


                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>


                        <div className='row border-box p-2'>
                            <div className='col-md-2 text-start'>
                                <label className="cursor-pointer auth-footer-link">
                                    <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                </label>                                    
                                <span className='checkbox-text'>FBC 1003</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Rahul Suprabhat</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Bengaluru Tech Team</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>Full Stack developer</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>rahul.suprabhat@gmail.com</span>
                            </div>
                            <div className='col-md-2'>
                                <span className='checkbox-text'>9811203256</span>
                            </div>
                        </div>

                        

                    </div>
                </div>
            </div>

        </div>

      </div>
    )
  }
}
