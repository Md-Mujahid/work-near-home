import Link from 'next/link'
import React, { Component } from 'react'

export class BookingComponent extends Component {
  render() {
    return (
        <div data-Component="booking">
            <div className='row'>
                <div className='col-md-12'>
                    <p className='booking-heading'>Booking</p>
                </div>

                <div className='col-md-12 mt-4'></div>

                <div className='col-md-12 mt-5 text-center'>
                    <p className='booking-sub-heading'>No bookings found :(</p>
                </div>

                <div className='col-md-12 text-center'>
                    <p className='booking-text'>Looks like your booking cart is empty at the moment. Get started by adding a new booking</p>
                </div>

                <div className='col-md-12 mt-4 mb-3 text-center'>
                    <Link href={`/booking/${`new-booking`}`} target="_self">
                        <button class="btn btn-primary new-booking-btn"  type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-plus-lg plus-icon" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                            </svg>
                            New Booking
                        </button>
                    </Link>
                </div>

                <div className='col-md-12 mb-2 text-center'>
                    <img src='/images/booking-img.png' className='img-fluid booking-page-img' alt='BOOKING'/>
                </div>

            </div>
        </div>
    )
  }
}

export default BookingComponent