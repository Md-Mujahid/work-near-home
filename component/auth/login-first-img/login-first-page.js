import React, { Component } from 'react'

export default class LoginFirstPage extends Component {
  render() {
    return (
      <div data-Component="login-first-page">
        <div className='row pl-0'>
          <div className='col-lg-12 col-md-12 col-sm-12'>
            <div className="img-cotainer text-center">
              <img src='/images/LM+PNG.png' alt='Leasing Monk' className='img-fluid login-page-img'/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
