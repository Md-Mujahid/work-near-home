import React, { Component } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import Link from 'next/link';
import SignUp from '../signup/sign-up';
import login from  '../../../src/login.json'


export class LoginComponent extends Component {
    constructor(props){
        super(props);
        this.state ={
            input: {
                email: '',
                password: '' 
            },
            isChecked: false,
        }
    }


    onChangeCheckbox = event => {
        this.setState({
            isChecked: event.target.checked
        })
    }

    handleChange = event => {
       let input = this.state.input;
       input[event.target.name] = event.target.value;
       this.setState({
        input,
       });
    }

    onSubmit = event =>{
        event.preventDefault()
    }


  render() {
    console.log(this.state.input, "123")
    return<section>
        <div data-Component="login-component"> 
            <div className='card card-padding'>
                <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <h1 className="auth-title">Log In</h1>
                        <p className="auth-sub-title">Log in with your data that you entered during Your registration</p>
                    {login.map(value =>{
                        return (
                        <form onChange={this.onSubmit}>
                            <div class="mb-3">
                                <label htmlFor="loginInputEmail" class="form-label">Email ID</label>
                                <input type="text" name='email' value={value.email_id} onChange={this.handleChange} class="form-control" id="loginInputEmail"/>
                            </div>
                            <div class="mb-3">
                                <label htmlFor="loginInputPassword" class="form-label">Password</label>
                                <input type="password" name='password' value={value.password} onChange={this.handleChange} class="form-control" id="loginInputPassword"/>
                            </div>
                            <div className="mb-3">
                                <div className="row">
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <label className="cursor-pointer auth-footer-link">
                                            <input className='form-check-input' type="checkbox" value="remember-me" onChange={this.onChangeCheckbox} id="remember_me"></input>
                                            <span> Remember me</span>
                                        </label>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6 text-end">
                                        <Link href="signup">
                                            <a className="forget-pass-link"> Forgot password?</a>
                                        </Link>
                                    </div>                                
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12">
                                    <div class="d-grid gap-2">
                                        <button class="btn btn-primary login-btn" type="button">Log In</button>
                                    </div>
                                </div>

                                <div className="col-lg-12 col-md-12 col-sm-12 mt-2">
                                    <span>Don't have an account yet?  <Link href="/signup"><a className="forget-pass-link">Sign Up</a></Link></span>
                                </div>

                            </div>
                        </form>)
                    })}
                    </div>
                </div>
            </div>    
        </div>
    </section>
  }
}

export default LoginComponent