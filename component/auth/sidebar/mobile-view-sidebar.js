import React, { Component } from 'react'
import { Offcanvas } from 'react-bootstrap';
import Sidebar from './sidebar';

export class MobileViewSidebar extends Component {
    constructor(props){
        super(props);
        this.state ={
            isOpen: false
        }
    }

    toggleModalClose = () =>{
        this.setState({
            isOpen: false
        })
    }

    toggleModal = () =>{
        this.setState({
            isOpen: true
        })
    }

  render() {
    return (
        <div data-Component="mobile-view-sidebar">
            <div className='row'>
                <div className='col-md-12 col-lg-12 col-sm-12'>
                    <img src='/favicon.ico' className='drawer' onClick={this.toggleModal}/>
                </div>
            </div>


        <Offcanvas show={this.state.isOpen} onHide={this.toggleModalClose} >
          <Offcanvas.Header closeButton>
            <Offcanvas.Title>More Filters</Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <Sidebar no={this.toggleModalClose}/>
          </Offcanvas.Body>
        </Offcanvas>

        </div>
    )
  }
}

export default MobileViewSidebar