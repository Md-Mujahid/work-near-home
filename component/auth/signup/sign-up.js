import Link from 'next/link'
import React, { Component } from 'react'

export class SignUp extends Component {
  render() {
    return (
        <div data-Component="sign-up">
            <div className='card card-padding'>
                <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <h1 className="auth-title mb-3">Sign Up</h1>
                        <form>
                            <div className='row'>
                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputFirstName" class="form-label">First name</label>
                                        <input type="text" name='firstname' class="form-control" id="loginInputFirstName"/>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputLastName" class="form-label">Last name</label>
                                        <input type="text" name='lastname' class="form-control" id="loginInputLastName"/>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputEmail" class="form-label">Email</label>
                                        <input type="text" name='email' class="form-control" id="loginInputEmail"/>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputLastName" class="form-label">Contact Number</label>
                                        <div class="input-group">
                                            <span className='dropdown-border'>
                                                <select class="form-select" id="inlineFormSelectPref">
                                                    <option selected>+91</option>
                                                    <option value="1">+966</option>
                                                    <option value="2">+92</option>
                                                <option value="3">+900</option>
                                                </select>
                                            </span>
                                            <input type="number" name='phonenmbr' class="form-control" id="inlineFormInputGroupUsername" placeholder="9812105025"/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputCompany" class="form-label">Company</label>
                                        <input type="text" name='company' class="form-control" id="loginInputCompany"/>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputUrl" class="form-label">LinkedIn Profile Url</label>
                                        <input type="Url" name='Url' class="form-control" id="loginInputUrl"/>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputPassword" class="form-label">Password</label>
                                        <input type="password" name='Password' class="form-control" id="loginInputPassword"/>
                                    </div>
                                    <p className='small-text'>password should be minimum 8 characters. It should not contain any special characters and spaces</p>
                                </div>

                                <div className='col-lg-6 col-md-12 col-sm-6'>
                                    <div class="mb-3">
                                        <label htmlFor="loginInputConfirmPassword" class="form-label">Confirm Password</label>
                                        <input type="password" name='ConfirmPassword' class="form-control" id="loginInputConfirmPassword"/>
                                    </div>
                                </div>

                            </div>
                            
                            <div className="mb-3">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12">
                                        <label className="cursor-pointer auth-footer-link">
                                            <input className='form-check-input' type="checkbox" value="remember-me" id="remember_me"></input>
                                            <span> By signing up, I agree to all the <a className="forget-pass-link">Terms & Condition and Privacy policy</a></span>
                                        </label>
                                    </div>                            
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12">
                                    <div class="d-grid gap-2">
                                        <button class="btn btn-primary login-btn" type="button">Sign Up</button>
                                    </div>
                                </div>

                                <div className="col-lg-12 col-md-12 col-sm-12 mt-2">
                                    <span>Already an existing user?  <Link href="/login"><a className="forget-pass-link">Log In</a></Link></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
        </div>
    )
  }
}

export default SignUp