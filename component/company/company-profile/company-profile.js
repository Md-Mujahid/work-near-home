import React, { Component } from 'react'

export class CompanyProfile extends Component {
  render() { 
    return (
        <div data-Component="company-profile">
            <div className='company-profile-page mb-4'>

                <div className='row head-row'>
                    <div className='col-md-12'>
                        <div className='row'>
                            <div className='col-md-10'>
                                <p className='company-heading'>Company Profile</p>
                            </div>
                            <div className='col-md-2 text-end'>
                                <button className='btn btn-primary edit-btn'>Edit details</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='company-page mb-5'> 
                    <div className='row'>
                        <div className='col-md-12 mt-5'>
                            <div class="card card-size">
                              <div class="card-body">
                                <p class="card-title">Company Detail</p>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <img src='/images/elevation.jpg' class="card-img-top card-img" alt="Card Img" />
                                    </div>
                                    <div className='col-md-6'>
                                        <p class="card-text">
                                            <span className='building-icon'>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-building" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694 1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"/>
                                                    <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"/>
                                                </svg>
                                            </span>
                                            <span className="elevation-text">Elevation Capital</span>
                                        </p>
                                        <p class="card-text mb-0">
                                            <span>
                                                <img src='/images/msoffice.png' className='card-space-icon'/>
                                            </span>
                                            <span className='address-text'>Bengaluru, India </span>
                                        </p>

                                    </div>  

                                    <div className='col-md-5 mt-3'>
                                        <p class="card-text mb-0">
                                            <span className='mail-background'>
                                                <span className='mail-icon'>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                                    </svg>
                                                </span>
                                                <span className='mail-text'>
                                                    contact@elevationyopmail.com
                                                </span>
                                            </span>    
                                        </p>
                                    </div> 

                                    <div className='col-md-4 mt-3'>
                                        <p class="card-text mb-0">
                                            <span className='call-background'>
                                                <span className='call-icon'>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                                    </svg>
                                                </span>
                                                <span className='call-text'>
                                                    +91 9854698752
                                                </span>
                                            </span>    
                                        </p>
                                    </div> 

                                    <div className='col-md-3 mt-3'>
                                        <p class="card-text mb-0">
                                            <span className='linkedin-background'>
                                                <span className='linkedin-icon'>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                                        <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                                                    </svg>
                                                </span>
                                                <span className='linkedin-text'>
                                                    Linkedin
                                                </span>
                                            </span>
                                        </p>
                                    </div>                                   
                                </div>
                                
                              </div>
                            </div>
                        </div>

                        <div className='col-md-12 mt-5 mb-5'>
                            <div class="card card-size-i">
                                    <div class="card-body">
                                        <p class="card-title mb-3">Spokeperson #1</p>
                                        <div className='row'>
                                            <div className='col-md-4'>
                                                <img src='/images/pixomatic_1572877223091.png' class="card-img-top profile-card-img" alt="Card Img" />
                                            </div>
                                            <div className='col-md-8'>
                                                <p class="card-text"><span className="elevation-text">Elevation Capital</span></p>
                                                <p class="card-text"><span className="designation-text">Senior HR, Elevation Capital</span></p>
                                                <p class="card-text mb-0"><span className='address-text'>Bengaluru, India </span></p>

                                            </div>  

                                            <div className='col-md-12 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='mail-background'>
                                                        <span className='mail-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='mail-text'>
                                                            contact@elevationyopmail.com
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='call-background'>
                                                        <span className='call-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                                                                <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='call-text'>
                                                            +91 9854698752
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='linkedin-background'>
                                                        <span className='linkedin-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                                                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='linkedin-text'>
                                                            Linkedin
                                                        </span>
                                                    </span>
                                                </p>
                                            </div>                                   
                                        </div>
                                        
                                    </div>
                            </div>

                            <div class="card card-size-i">
                                    <div class="card-body">
                                        <p class="card-title mb-3">Spokeperson #1</p>
                                        <div className='row'>
                                            <div className='col-md-4'>
                                                <img src='/images/pixomatic_1572877223091.png' class="card-img-top profile-card-img" alt="Card Img" />
                                            </div>
                                            <div className='col-md-8'>
                                                <p class="card-text"><span className="elevation-text">Elevation Capital</span></p>
                                                <p class="card-text"><span className="designation-text">Senior HR, Elevation Capital</span></p>
                                                <p class="card-text mb-0"><span className='address-text'>Bengaluru, India </span></p>

                                            </div>  

                                            <div className='col-md-12 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='mail-background'>
                                                        <span className='mail-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='mail-text'>
                                                            contact@elevationyopmail.com
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='call-background'>
                                                        <span className='call-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                                                                <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='call-text'>
                                                            +91 9854698752
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='linkedin-background'>
                                                        <span className='linkedin-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                                                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='linkedin-text'>
                                                            Linkedin
                                                        </span>
                                                    </span>
                                                </p>
                                            </div>                                   
                                        </div>
                                        
                                    </div>
                            </div>

                            <div class="card card-size-i">
                                    <div class="card-body">
                                        <p class="card-title mb-3">Spokeperson #1</p>
                                        <div className='row'>
                                            <div className='col-md-4'>
                                                <img src='/images/pixomatic_1572877223091.png' class="card-img-top profile-card-img" alt="Card Img" />
                                            </div>
                                            <div className='col-md-8'>
                                                <p class="card-text"><span className="elevation-text">Elevation Capital</span></p>
                                                <p class="card-text"><span className="designation-text">Senior HR, Elevation Capital</span></p>
                                                <p class="card-text mb-0"><span className='address-text'>Bengaluru, India </span></p>

                                            </div>  

                                            <div className='col-md-12 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='mail-background'>
                                                        <span className='mail-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                                                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='mail-text'>
                                                            contact@elevationyopmail.com
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='call-background'>
                                                        <span className='call-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                                                                <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='call-text'>
                                                            +91 9854698752
                                                        </span>
                                                    </span>    
                                                </p>
                                            </div> 

                                            <div className='col-md-6 mb-2 mt-3'>
                                                <p class="card-text mb-0">
                                                    <span className='linkedin-background'>
                                                        <span className='linkedin-icon'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                                                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                                                            </svg>
                                                        </span>
                                                        <span className='linkedin-text'>
                                                            Linkedin
                                                        </span>
                                                    </span>
                                                </p>
                                            </div>                                   
                                        </div>
                                        
                                    </div>
                            </div>

                        </div> 


                    </div>
                </div>

            </div>
        </div>
    )
  }
}

export default CompanyProfile