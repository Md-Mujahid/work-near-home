import Link from 'next/link'
import React, { Component } from 'react'

export class EditComponyProfile extends Component {
  render() {
    return (
        <div data-Component="edit-compony-profile">
            <div className='edit-company-page mb-4'>

                <div className='row mb-2'>
                    <div className='col-lg-9 col-md-9 col-sm-9 col mt-3'>
                        <p className='company-heading'>Edit Company Profile</p>
                    </div>
                    <div className='col col-dispaly'></div>
                    <div className='col-lg-1 col-md-1 col-sm-1 col text-end mt-3'>
                        <Link href='/dashboard'>
                            <button class="btn btn-primary cancel-btn" type="button">Cancel</button>
                        </Link>
                    </div>
                    <div className='col-lg-2 col-md-2 col-sm-2 col text-end mt-3'>
                        <button class="btn btn-primary save-btn" type="button">Save changes</button>
                    </div>
                </div>

                <div className='edit-company-form-page scroll'>
                    <div className='row'>

                        <div className='col-lg-12 col-md-12 col-sm-12'>
                            <div className='heading-bottom-border'>
                                <h1 className='company-sub-heading mb-0'>Company Details</h1>
                            </div>
                        </div>

                        <form>
                            <div className='form-field'>
                                <div className='row'>
                                    <div className='col-lg-3 col-md-3 col-sm-3 mt-5 pt-4 pb-4'>
                                        <div class="mb-3 dot">
                                            <input type="file" name='file' class="form-control" hidden id="imgupload"/>
                                            <label htmlFor="imgupload"><img src="/images/upload.svg" /></label>
                                            <div>
                                                <p className="upload-text">Add Company Logo</p>
                                                <p className='upload-text-I'> Upto 5 mb | JPEG/PNG</p>    
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div className='col-lg-9 col-md-9 col-sm-9 mt-4'>
                                        <div className='row'>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputFirstName" class="form-label">Company Name</label>
                                                        <input type="text" name='firstname' class="form-control" id="loginInputFirstName" placeholder='Corporate Banking Pvt. Ltd.'/>
                                                    </div>
                                                </div>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputLastName" class="form-label">Company's GST</label>
                                                        <input type="text" name='lastname' class="form-control" id="loginInputLastName" placeholder='04AABCU9603R1ZV'/>
                                                    </div>
                                                </div>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputEmail" class="form-label">Company's official Email ID</label>
                                                        <input type="text" name='email' class="form-control" id="loginInputEmail" placeholder='contact@corporatebanking.com'/>
                                                    </div>
                                                </div>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="inlineFormInputGroupUsername" class="form-label">Company's official Contact Number</label>
                                                        <div class="input-group">
                                                            <span className='dropdown-border'>
                                                                <select class="form-select border-0" id="inlineFormSelectPref">
                                                                    <option selected>+91</option>
                                                                    <option value="1">+966</option>
                                                                    <option value="2">+92</option>
                                                                    <option value="3">+900</option>
                                                                </select>
                                                            </span>
                                                            <input type="number" name='phonenmbr' class="form-control" id="inlineFormInputGroupUsername" placeholder="9812105025"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputHeadquaters" class="form-label">Company Headquaters</label>
                                                        <input type="text" name='lastname' class="form-control" id="loginInputHeadquaters" placeholder='Bengaluru, India'/>
                                                    </div>
                                                </div>

                                                <div className='col-lg-6 col-md-6 col-sm-6'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputLinkedIn" class="form-label">LinkedIn Url</label>
                                                        <input type="text" name='email' class="form-control" id="loginInputLinkedIn" placeholder='https://www.linkedin.com/in/benjamin-grant-72381ujy3u.'/>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>    

                        <div className='col-lg-12 col-md-12 col-sm-12 mt-5'>
                            <div className='card'>
                                <div className='profile-bottom-border'>
                                    <div className='row spokeperson-heading-padding'>
                                        <div className='col-lg-11 col-md-11 col-sm-11 col'>
                                            <p className='spokeperson-heading'>Spokeperson Details #1</p>
                                        </div>
                                        <div className='col col-display'></div>
                                        <div className='col col-display'></div>
                                        <div className='col col-display'></div>
                                        <div className='col-lg-1 col-md-1 col-sm-1 col'>
                                            <span className='plus-circle-icon'>
                                                <Link href="/booking">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                    </svg>
                                                </Link>
                                            </span>
                                            <span className='delete-icon'>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <form>
                                    <div className='profile-bottom-border border-0 mt-4 mb-3'>
                                        <div className='row'>

                                            <div className='col-lg-2 col-md-2 col-sm-2'>
                                                <img src='/images/pixomatic_1572877223091.png' alt='Profile Img' className='profile-img'/>
                                                <p className='mb-0'>
                                                    <input type="file" name='file' class="form-control" hidden id="imgupload"/>
                                                    <label htmlFor="imgupload" className='profile-img-text'>Edit profile images</label>
                                                </p>    
                                            </div>

                                            <div className='col-lg-9 col-md-9 col-sm-9'>
                                                <div className='row'>
                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                        <div class="mb-3">
                                                            <label htmlFor="loginInputFullName" class="form-label">Full Name</label>
                                                            <input type="text" name='fullname' class="form-control" id="loginInputFullName" placeholder='Jhon Doe (You)'/>
                                                        </div>
                                                    </div>

                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                    <div class="mb-3">
                                                        <label htmlFor="loginInputTeam" class="form-label">Team</label>
                                                        <input type="text" name='team' class="form-control" id="loginInputTeam" placeholder='Bengaluru Hr Team'/>
                                                    </div>

                                                    </div>
                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                        <div class="mb-3">
                                                            <label htmlFor="loginInputDesignation" class="form-label">Designation</label>
                                                            <input type="text" name='designation' class="form-control" id="loginInputDesignation" placeholder='Senior Hr'/>
                                                        </div>
                                                    </div>

                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                        <div class="mb-3">
                                                            <label htmlFor="loginInputEmailID" class="form-label">Email ID</label>
                                                            <input type="text" name='emailid' class="form-control" id="loginInputEmailID" placeholder='Corporate Banking Pvt. Ltd.'/>
                                                        </div>
                                                    </div>

                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                        <div class="mb-3">
                                                            <label htmlFor="inlineFormInputGroupUsername" class="form-label">Contact Number</label>
                                                            <div class="input-group">
                                                                <span className='dropdown-border'>
                                                                    <select class="form-select border-0" id="inlineFormSelectPref">
                                                                        <option selected>+91</option>
                                                                        <option value="1">+966</option>
                                                                        <option value="2">+92</option>
                                                                        <option value="3">+900</option>
                                                                    </select>
                                                                </span>
                                                                <input type="number" name='phonenmbr' class="form-control" id="inlineFormInputGroupUsername" placeholder="9812105025"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className='col-lg-4 col-md-4 col-sm-4'>
                                                        <div class="mb-3">
                                                            <label htmlFor="loginInputLinkedInProfileUrl" class="form-label">LinkedIn Profile Url</label>
                                                            <input type="text" name='linkedinprofileurl' class="form-control" id="loginInputLinkedInProfileUrl" placeholder='https://www.linkedin.com/in/benjamin-grant-72381ujy3u.'/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>  
                                    </div>
                                </form>

                            </div>
                        </div>  

                    </div>
                </div>


            </div>
        </div>
    )
  }
}

export default EditComponyProfile