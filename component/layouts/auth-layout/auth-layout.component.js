import Head from 'next/head'
import React from 'react'
import Styles from '../../../styles/auth-layout.module.scss';
import { ToastContainer } from 'react-bootstrap'

export default function AuthLayoutComponent({ children, home }) {
  return (
    <div className={Styles.body}>
        <Head>
            <link rel="icon" href="/favicon.ico" />
            {/* <meta
                name="description"
                content={APP_TAGLINE}
            />
            <meta
                property="og:image"
                content={APP_IMAGE}
            />
            <meta name="og:title" content={APP_NAME} />
            <meta name="twitter:card" content="summary_large_image" /> */}
        </Head>
        <main class="auth-layout-body">
            <div class="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div class="auth-container">
                            {/* <img src="/images/login-logo.png"  className="brand-logo" /> */}
                            <ToastContainer position="top-center" />
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
  )
}
