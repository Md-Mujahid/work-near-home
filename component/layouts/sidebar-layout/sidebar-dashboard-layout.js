import Head from 'next/head'
import React from 'react'
import { ToastContainer } from 'react-bootstrap'
import MobileViewSidebar from '../../auth/sidebar/mobile-view-sidebar'
import Sidebar from '../../auth/sidebar/sidebar'

export default function SidebarDashboardLayout({children, home,}) {
  return (
    <div>
        <Head>
            <link rel="icon" href="/favicon.ico" />
        </Head>
        <main data-Component="layout-lm-dashboard" className="layout-lm-dashboard">
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />

            <div className="dashboard-container">
                <div className='sidebar-container'>
                    <Sidebar/>
                </div>

                <div className='sidebar-container-second'>
                    <MobileViewSidebar/>
                </div>
                    
                <div className='main-body scroll pl-0 pr-0'>
                    {children}
                </div> 
            </div>

        </main>
    </div>
  )
}
